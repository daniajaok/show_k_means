const navbarToggler = document.getElementById('navbar-toggler');
const navbarItem = document.querySelectorAll('.navbar__item');
const navbarDropdown = document.querySelectorAll('.navbar__dropdown')

navbarToggler.addEventListener('click', (e) => {
    const icon = navbarToggler.querySelector('i');
    if (!navbarToggler.className.includes('active')){
        navbarToggler.classList.add('active')
        icon.classList.remove('bxs-grid')
        icon.classList.add('bxs-grid-alt')
    }else{
        navbarToggler.classList.remove('active')
        icon.classList.remove('bxs-grid-alt')
        icon.classList.add('bxs-grid')
    }
})

navbarItem.forEach(navItem => {
    navItem.addEventListener('click', (e) => {
        if (navbarToggler.className.includes('active') && !navItem.className.includes('navbar__dropdown')){
            navbarToggler.classList.remove('active')
        }
    })
})

function deleteActiveDropdown() {
    navbarDropdown.forEach(activeDropdown => {
        activeDropdown.classList.remove('active')
    })
}
navbarDropdown.forEach(navDorpdown => {
    navDorpdown.addEventListener('click', (e) => {
        if(navDorpdown.className.includes('active')){
            navDorpdown.classList.remove('active')
        }else{
            deleteActiveDropdown();
            navDorpdown.classList.add('active')
        }
    })
})
