const downloadEXCELCluster = document.querySelectorAll('.download_excel_cluster')

downloadEXCELCluster.forEach((excelCluster, index) => {
    excelCluster.addEventListener('click', (e) => {
        e.preventDefault();
        var cluster = 'cluster' + index
        loadingWrapper.classList.add('active');
        var url = `/download-and-show-data-cluster?cluster=${cluster}&download=1`;
        loadContent(url);
    })
})
