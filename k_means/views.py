from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import tarfile
from django.conf import settings
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from pandas_profiling import ProfileReport
from sklearn.cluster import SpectralClustering
import pandas as pd
import json


print('Starting Data Preparation ...')

# Data Web
DATA_WEB = {
    'upload_file': False
}

# Uncleaned Data
df = pd.read_csv(settings.BASE_DIR / 'static/dataset/DatasetTaV2.csv')

# Cleaned Data
def remove_outlier(df):
    Q1 = df.quantile(0.25)
    Q3 = df.quantile(0.75)
    IQR = Q3-Q1
    df_final = df[~(df>(Q1-(1.5*IQR)))|(df<(Q3+(1.5*IQR)))]
    return df_final

df2 = df.copy()
df2["Jumlah Pinjaman "] = (df2["Jumlah Pinjaman "] - df2["Jumlah Pinjaman "].min()) / (df2["Jumlah Pinjaman "].max() -df2["Jumlah Pinjaman "].min())
df2["Type Pinj"] = (df2["Type Pinj"] - df2["Type Pinj"].min()) / (df2["Type Pinj"].max() - df2["Type Pinj"].min())
df2["Suku Bunga"] = (df2["Suku Bunga"] - df2["Suku Bunga"].min()) / (df2["Suku Bunga"].max() - df2["Suku Bunga"].min())
df2["Hari Nunggak"] = (df2["Hari Nunggak"] - df2["Hari Nunggak"].min()) / (df2["Hari Nunggak"].max() - df2["Hari Nunggak"].min())
df2["Tunggakan Bunga"] = (df2["Tunggakan Bunga"] - df2["Tunggakan Bunga"].min()) / (df2["Tunggakan Bunga"].max() - df2["Tunggakan Bunga"].min())
df2["Tunggakan Pokok "] = (df2["Tunggakan Pokok "] - df2["Tunggakan Pokok "].min()) / (df2["Tunggakan Pokok "].max() - df2["Tunggakan Pokok "].min())
df2["Baki Debet "] = (df2["Baki Debet "] - df2["Baki Debet "].min()) / (df2["Baki Debet "].max() - df2["Baki Debet "].min())
df2["Masa Pinjaman JKW"] = (df2["Masa Pinjaman JKW"] - df2["Masa Pinjaman JKW"].min()) / (df2["Masa Pinjaman JKW"].max() - df2["Masa Pinjaman JKW"].min())
df2["JT"] = (df2["JT"] - df2["JT"].min()) / (df2["JT"].max() - df2["JT"].min())
df2 = remove_outlier(df2)
df2.dropna(axis=0,inplace=True)

# Refresh Data
df3 = df2.copy()
df3 = df3[['Masa Pinjaman JKW','Jumlah Pinjaman ','Baki Debet ','Hari Nunggak','JT']]
sc = StandardScaler()
standarSc = sc.fit_transform(df3)

# Cluster
Data_frame=df2.copy()
cluster0 = 'Init'

print('Completed Data Preparation ...')

def index(request):
    context = {
        'title_page': 'Upload File',
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'upload_file.html', context)

def uncleaned_data(request):
    if request.GET.get('upload_file', False):
        DATA_WEB['upload_file'] = True
    title_data = df.columns
    json_records = df.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title': 'Spectra Clastering | Uncleaned Data',
        'refresh_button': False,
        'title_page': 'Data Before Cleanup',
        'title_data': title_data,
        'content_data': data_csv,
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context=context)

def cleaned_data(request):
    if 'Hasil_Spectral_Clustering' in df2.columns:
        df2.drop(['Hasil_Spectral_Clustering'], axis=1, inplace=True)

    cluster_view = False

    if request.GET.get('refresh_data', False):
        sc = SpectralClustering(n_clusters=5, eigen_solver=None, n_components=None,
                            random_state=74, n_init=10, gamma=1.0, affinity='nearest_neighbors', 
                            n_neighbors=10, eigen_tol=0.0, assign_labels='kmeans',
                            degree=3, coef0=1, kernel_params=None, n_jobs=None, verbose=False).fit(standarSc)
        label = sc.labels_
        df2['Hasil_Spectral_Clustering'] = label
        cluster_view = True

    title_data = df2.columns
    json_records = df2.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title': 'Spectra Clastering | Cleaned Data',
        'refresh_button': True,
        'title_page': 'Data After Cleanup',
        'title_data': title_data,
        'content_data': data_csv,
        'cluster_view': cluster_view,
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context=context)

def download_html(request):
    profile = ProfileReport(df2, title="Spectral Clustering Report")
    profile.to_file('Report.html')
    response = HttpResponse(content_type='text/html')
    response['Content-Disposition'] = 'attachment; filename=Report.tar.gz'
    tarred = tarfile.open(fileobj=response, mode='w:gz')
    tarred.add('Report.html')
    tarred.close()
    return response

def download_excel(request):
    df2.to_excel("hasil_ahir_SpectraClastering.xlsx")
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=hasil_ahir_SpectraClastering.tar.gz'
    tarred = tarfile.open(fileobj=response, mode='w:gz')
    tarred.add('hasil_ahir_SpectraClastering.xlsx')
    tarred.close()
    return response

def download_and_show_data_cluster(request):
    cluster = request.GET.get('cluster', None)
    download_cluster = request.GET.get('download', False)
    data_cluster = Data_frame
    sub_title = 'Cluster'
    data_query_param = cluster
    cluster_view = True
    title_data_cluster = ''

    # Setup Cluster
    sc = SpectralClustering(n_clusters=5, eigen_solver=None, n_components=None,
                            random_state=74, n_init=10, gamma=1.0, affinity='nearest_neighbors', 
                            n_neighbors=10, eigen_tol=0.0, assign_labels='kmeans',
                            degree=3, coef0=1, kernel_params=None, n_jobs=None, verbose=False).fit(standarSc)
    label = sc.labels_
    Data_frame['Cluster']=label
    if cluster == 'cluster0':
        title_data_cluster = 'SpectraClastering0'
        sub_title = 'Cluster 0'
        data_cluster = Data_frame[Data_frame['Cluster'] == 0]
    elif cluster == 'cluster1':
        title_data_cluster = 'SpectraClastering1'
        sub_title = 'Cluster 1'
        data_cluster = Data_frame[Data_frame['Cluster'] == 1]
    elif cluster == 'cluster2':
        title_data_cluster = 'SpectraClastering2'
        sub_title = 'Cluster 2'
        data_cluster = Data_frame[Data_frame['Cluster'] == 2]
    elif cluster == 'cluster3':
        title_data_cluster = 'SpectraClastering3'
        sub_title = 'Cluster 3'
        data_cluster = Data_frame[Data_frame['Cluster'] == 3]
    elif cluster == 'cluster4':
        title_data_cluster = 'SpectraClastering4'
        sub_title = 'Cluster 4'
        data_cluster = Data_frame[Data_frame['Cluster'] == 4]

    if download_cluster == '1': # Download data cluster
        data_cluster.to_excel("%s.xlsx" % (title_data_cluster))
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = 'attachment; filename=%s.tar.gz' % (title_data_cluster)
        tarred = tarfile.open(fileobj=response, mode='w:gz')
        tarred.add('%s.xlsx' % (title_data_cluster))
        tarred.close()
        return response
    else: # View data cluster
        title_data = data_cluster.columns
        json_records = data_cluster.reset_index().to_json(orient='records')
        data_csv = json.loads(json_records)
        context = {
            'title': 'Spectra Clastering | %s' % (sub_title),
            'refresh_cluster_button': True,
            'title_page': sub_title,
            'title_data': title_data,
            'content_data': data_csv,
            'cluster_view': cluster_view,
            'data_query_param': data_query_param,
            'upload_file': DATA_WEB['upload_file'],
        }
        return render(request, 'index.html', context=context)
