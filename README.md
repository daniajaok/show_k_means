# Aplikasi menganalisis sebaran wilayah yang sering menunggak pembayaran di daerah Cikalong. Metode yang digunakan untuk analisis adalah K-Means Clustering

Sebuah aplikasi web berbasis Django telah dikembangkan untuk menganalisis sebaran wilayah yang sering menunggak pembayaran di daerah Cikalong. Metode yang digunakan untuk analisis adalah K-Means Clustering.

Framework Django digunakan untuk membangun platform yang interaktif dan mudah digunakan bagi pengguna akhir. Aplikasi ini memungkinkan pengguna untuk memasukkan data pembayaran tunggakan dari wilayah-wilayah di Cikalong.

Setelah pengguna memasukkan data, sistem menggunakan algoritma K-Means Clustering untuk mengelompokkan wilayah-wilayah tersebut berdasarkan pola pembayaran tunggakan. Algoritma ini akan mengidentifikasi kelompok wilayah yang memiliki pola yang serupa dalam hal pembayaran tunggakan.

Hasil analisis tersebut kemudian disajikan kembali kepada pengguna melalui antarmuka web yang ramah pengguna. Pengguna dapat melihat visualisasi sebaran wilayah berdasarkan kelompok-kelompok yang dihasilkan oleh K-Means Clustering. Informasi tambahan seperti statistik tentang setiap kelompok wilayah juga disediakan, seperti rata-rata tunggakan, variasi, dan lain-lain.

Selain itu, aplikasi ini dilengkapi dengan fitur untuk melakukan prediksi terhadap wilayah-wilayah yang mungkin akan mengalami tunggakan di masa mendatang berdasarkan pola pembayaran tunggakan yang telah teridentifikasi. Hal ini memberikan nilai tambah bagi pihak yang berwenang untuk mengambil tindakan preventif secara proaktif.

Secara keseluruhan, aplikasi web ini menyediakan alat analisis yang kuat dan berbasis data untuk membantu pihak terkait dalam mengelola dan mengatasi masalah pembayaran tunggakan di daerah Cikalong, dengan menggunakan teknologi Django dan algoritma K-Means Clustering

### Note 
jika anda ingin mejalankanya aplikasi tolong hubingi saya [Muhammad Ramdhani](wa.me/+6281399057525) karena aplikasi ini ada data privasi yang tidak saya sertakan pada repository ini